CXX = g++
CXXFLAGS = -g -Wall -Wextra

program : romandigitconverter.cpp numberconversion.o romandigitconverter.o
	$(CXX) $(CXXFLAGS) -o program

numberconversion.o : numberconversion.cpp numberconversion.h
	$(CXX) $(CXXFLAGS) -c numberconversion.cpp

clean :
	rm *.o
	rm program
